
# ckanext-sso
OpenID connect plugin for ISSI-CKAN

## Requirements
- Ckan 2.9

## Installation
To install
1) activate your virtual environment ie `source venv/bin/activate`
2) Install the requirements `pip install -r requirements.txt`
3) Install the package `python setup.py install`
4) Add `sso` settings in CKAN config file
```
ckan.plugins = sso {OTHER PLUGINS}
ckan.sso.redirect_uri = https://ckanserver/
ckan.sso.keycloak_url = https://keycloakserver/
ckan.sso.client_id = <client id>
ckan.sso.client_secret = <client secret>
```
5) Restart CKAN if it was already running
