import logging
from ckan.lib import base
from ckan.views.user import set_repoze_user
import ckan.logic
from flask import session

from ckan import plugins
from ckan.plugins import toolkit
from requests_oauthlib import OAuth2Session

import secrets
import re

import ckan.model as model
from ckan.lib.helpers import redirect_to as redirect

log = logging.getLogger(__name__)

# Ckan does not allow other characters for user name.
FORBIDDEN_NAME_CHARACTERS_RE = re.compile("[^a-z0-9_\-]")


def get_valid_user_name(user_name):
    return FORBIDDEN_NAME_CHARACTERS_RE.sub("_", user_name)


class SSOPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IAuthenticator, inherit=True)
    plugins.implements(plugins.IConfigurable)

    def __init__(self, name=None):
        pass
        # self.sso_helper = helper.SSOHelper()

    def configure(self, config):
        required_keys = (
            "ckan.sso.redirect_uri",
            "ckan.sso.keycloak_url",
            "ckan.sso.client_id",
            "ckan.sso.client_secret",
        )
        for key in required_keys:
            if config.get(key) is None:
                raise RuntimeError(
                    "Required configuration option {0} not found.".format(key)
                )
        self.redirect_uri = config.get("ckan.sso.redirect_uri")
        self.keycloak_url = config.get("ckan.sso.keycloak_url")
        self.client_id = config.get("ckan.sso.client_id")
        self.client_secret = config.get("ckan.sso.client_secret")
        self.realm = "master"
        self.scope = ["email", "profile", "roles"]
        self.profile_username_field = "preferred_username"
        self.profile_email_field = "email"
        self.authorization_endpoint = (
            f"{self.keycloak_url}realms/{self.realm}/protocol/openid-connect/auth"
        )
        self.token_endpoint = (
            f"{self.keycloak_url}realms/{self.realm}/protocol/openid-connect/token"
        )
        self.userinfo_endpoint = (
            f"{self.keycloak_url}realms/{self.realm}/protocol/openid-connect/userinfo"
        )
        self.admin_role = "issi_admin"
        self.user_roles = set(
            (
                "issi_admin",
                "issi_editor",
                "issi_metodik",
                "issi_redaktor",
                "issi_schvalovatel",
            )
        )

    def login(self):
        oauth = OAuth2Session(
            self.client_id, redirect_uri=self.redirect_uri, scope=self.scope
        )
        authorization_url, state = oauth.authorization_url(self.authorization_endpoint)
        return toolkit.redirect_to(authorization_url)

    def logout(self):
        if "user_id" in session:
            del session["user_id"]
            session.save()

    def identify(self):
        if "user_id" in session:
            toolkit.g.user = session["user_id"]
            toolkit.g.userobj = model.User.by_name(toolkit.g.user)
            return None

        if toolkit.request.args.get("session_state", None):
            oauth = OAuth2Session(
                self.client_id, redirect_uri=self.redirect_uri, scope=self.scope
            )
            # We are logging in.
            token = oauth.fetch_token(
                self.token_endpoint,
                authorization_response=toolkit.request.url,
                client_secret=self.client_secret,
            )
            user_info = oauth.get(self.userinfo_endpoint).json()
            user_name = get_valid_user_name(user_info[self.profile_username_field])
            user_email = user_info[self.profile_email_field]
            roles = set(user_info["realm_access"]["roles"])
            is_admin = self.admin_role in roles
            is_user = bool(roles & self.user_roles)
            if is_user:
                user = self._get_or_create_user(user_name, user_email)
                if user:
                    toolkit.g.user = user.get("name")
                    toolkit.g.userobj = model.User.by_name(toolkit.g.user)

                    if is_admin != toolkit.g.userobj.sysadmin:
                        toolkit.g.userobj.sysadmin = is_admin
                        toolkit.g.userobj.save()

                    response = toolkit.redirect_to(self.redirect_uri)
                    session["user_id"] = toolkit.g.user
                    return response
            else:
                # User does not have any role
                log.info("User %s does not have any role. Not logging in.", user_name)
                self.logout()
                response = toolkit.redirect_to(self.redirect_uri)
                return response
        return None

    def _get_or_create_user(self, user_name, user_email):
        context = self._prepare_context()

        user_obj = None
        try:
            user_obj = toolkit.get_action("user_show")(context, {"id": user_name})
        except ckan.logic.NotFound:
            pass
        if user_obj:
            log.info("User %s found. Logging in.", user_name)
            return user_obj
        else:
            log.info("User %s not found. Creating new user and logging in.", user_name)
            # Create user
            user_dict = {
                "name": user_name,
                "email": user_email,
                "full_name": user_name,
                "password": secrets.token_urlsafe(16),
                "sysadmin": False,
            }

            try:
                return toolkit.get_action("user_create")(context, user_dict)
            except toolkit.ValidationError as e:
                error_message = e.error_summary or e.message or e.error_dict
            base.abort(400, error_message)

    def _prepare_context(self):
        site_user = toolkit.get_action("get_site_user")(
            {"model": model, "ignore_auth": True}, {}
        )
        context = {
            "model": model,
            "session": model.Session,
            "ignore_auth": True,
            "user": site_user["name"],
        }
        return context
